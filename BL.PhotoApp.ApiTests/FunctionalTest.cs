using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BL.PhotoApp.ApiTests
{
    [TestClass]
    public class FunctionalTest
    {
        [TestMethod]
        public void ExceedingImageSizeShouldGiveABadRequest()
        {
        }

        [TestMethod]
        public void InvalidFileFormatShouldGiveABadRequest()
        {
        }

        [TestMethod]
        public void UploadedImagesShouldBeStoredInOriginalFileFormatAndJPGFormat()
        {
        }

        [TestMethod]
        public void RequestingForAnImageShouldServeOnlyJPGFormat()
        {
        }

        [TestMethod]
        public void PostsShouldBeSortedByNumberOfCommentsInDescendingOrder()
        {
        }
    }
}
