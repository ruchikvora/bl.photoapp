﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BL.PhotoApp.API.ViewModels
{
    public class PostViewModel
    {
        public PostViewModel()
        {
            Comments = new List<CommentViewModel>();
        }

        public int Id { get; set; }

        public string Username { get; set; }

        public string Image { get; set; }

        public ICollection<CommentViewModel> Comments { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}
