﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BL.PhotoApp.API.ViewModels
{
    public class CommentViewModel
    {
        public int Id { get; set; }

        public int PostId { get; set; }

        public string Username { get; set; }

        public string Text { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}
