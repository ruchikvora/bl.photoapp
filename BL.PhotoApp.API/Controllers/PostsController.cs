﻿using AutoMapper;
using BL.PhotoApp.API.Data;
using BL.PhotoApp.API.Helpers;
using BL.PhotoApp.API.Models;
using BL.PhotoApp.API.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BL.PhotoApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        AppDbContext dbContext;
        ImageConverter imageConverter;
        IMapper mapper;
        string jpgPath;
        string rawPath;
        public PostsController(AppDbContext dbContext, 
            ImageConverter imageConverter,
            IMapper mapper,
            IWebHostEnvironment env)
        {
            this.dbContext = dbContext;
            this.imageConverter = imageConverter;
            this.mapper = mapper;
            jpgPath = Path.Combine(env.ContentRootPath, "AppData", "jpg");
            rawPath = Path.Combine(env.ContentRootPath, "AppData", "raw");
        }

        [HttpGet]
        public async Task<IActionResult> GetPosts(int page = 1, int pageSize = 10)
        {
            var posts = await dbContext.Posts.Include("Comments").OrderByDescending(c => c.Comments.Count).Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
            posts.Where(c => c.Comments.Any()).ToList().ForEach(p => {
                p.Comments = p.Comments.OrderByDescending(c => c.CreatedOn).Take(2).ToList();
            });
            return Ok(mapper.Map<List<PostViewModel>>(posts));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetPost(int id)
        {
            var post = await dbContext.Posts.FindAsync(id);
            var comments = await dbContext.Comments.Where(c => c.PostId == id).OrderByDescending(c => c.CreatedOn).Take(2).ToListAsync();
            post.Comments = comments;
            return Ok(mapper.Map<PostViewModel>(post));
        }

        [HttpGet("{id}/img")]
        public async Task<IActionResult> ServeImage(int id)
        {
            var post = await dbContext.Posts.FindAsync(id);
            var fileName = post.Image;
            string path;
            if (IsJpg(fileName))
            {
                path = Path.Combine(rawPath, fileName);
            }
            else
            {
                var extension = Path.GetExtension(fileName);
                path = Path.Combine(jpgPath, fileName.Replace(Path.GetExtension(extension), ".jpg"));
            }
            FileStream stream = System.IO.File.OpenRead(path);
            return File(stream, "image/jpeg");
        }

        [HttpPost]
        public async Task<IActionResult> Post(IFormFile file, string username)
        {
            if (file == null)
                return BadRequest(new { message = "Image missing" });
            if (!IsFileFormatValid(file.FileName))
                return BadRequest(new { message = "Only .jpg, .png and .bmp file formats accepted" });
            if (!IsFileSizeLessThan100MB(file))
                return BadRequest(new { message = "Max. acceptable image size is 100MB" });
            
            var fileName = await SaveFile(file);
            var post = new Post 
            {
                CreatedOn = DateTime.UtcNow,
                Image = fileName,
                Username = username  
            };

            dbContext.Posts.Add(post);
            await dbContext.SaveChangesAsync();
            return Created(new Uri(Request.GetEncodedUrl() + "/" + post.Id), mapper.Map<PostViewModel>(post));
        }

        [HttpPost("{id}/comment")]
        public async Task<IActionResult> Comment(int id, string username, string text)
        {
            var post = await dbContext.Posts.FindAsync(id);
            if (post == null)
                return BadRequest(new { message = "Invalid post" });
            var comment = new Comment
            {
                CreatedOn = DateTime.UtcNow,
                Username = username,
                Text = text
            };
            post.Comments.Add(comment);
            await dbContext.SaveChangesAsync();
            return Created(new Uri(Request.GetEncodedUrl() + "/" + post.Id), mapper.Map<CommentViewModel>(comment));
        }

        [HttpDelete("{id}/comment")]
        public async Task<IActionResult> DeleteComments(int id, string username)
        {
            dbContext.Comments.RemoveRange(dbContext.Comments.Where(c => c.PostId == id && c.Username == username));
            await dbContext.SaveChangesAsync();
            return Ok();
        }

        private async Task<string> SaveFile(IFormFile file)
        {
            var fileName = $"{DateTime.UtcNow.Ticks}_{file.FileName}";
            var path = Path.Combine(rawPath, fileName);
            using (var stream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            if (!IsJpg(fileName))
            {
                await Task.Run(() =>
                {
                    var extension = Path.GetExtension(fileName);
                    imageConverter.SaveJpg(path, Path.Combine(jpgPath, fileName.Replace(extension, ".jpg")), 80);
                });
            }

            return fileName;
        }

        private bool IsFileFormatValid(string fileName)
        {
            var extension = Path.GetExtension(fileName).ToLower();
            return (extension == ".jpeg" || extension == ".jpg" || extension == ".png" || extension == ".bmp"); 
        }

        private bool IsJpg(string fileName)
        {
            var extension = Path.GetExtension(fileName).ToLower();
            return (extension == ".jpeg" || extension == ".jpg");
        }

        private bool IsFileSizeLessThan100MB(IFormFile file)
        {
            return file.Length <= 1e+8;
        }
    }
}
