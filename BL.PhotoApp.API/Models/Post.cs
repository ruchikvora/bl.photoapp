﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BL.PhotoApp.API.Models
{
    public class Post
    {
        public Post()
        {
            Comments = new List<Comment>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Username { get; set; }

        public string Image { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}
